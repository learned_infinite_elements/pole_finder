# This is a Python translation of the Matlab code 
# https://github.com/PioKow/GRPF (author: Piotr Kowalczyk)
# for computing poles and roots of meromorphic functions 

import numpy as np
from scipy.spatial import Delaunay,delaunay_plot_2d
import matplotlib.pyplot as plt
from math import ceil,pi

def vinq(f):
    if f.real > 0 and f.imag >= 0:
        return 1
    elif f.real <= 0 and f.imag >0:
        return 2
    elif f.real < 0 and f.imag <=0:
        return 3
    elif f.real >= 0 and f.imag <0:
        return 4
    else:
        return np.nan 

def rect_dom(xb,xe,yb,ye,r):
    X = xe-xb
    Y = ye-yb
    
    n = ceil(Y/r+1)
    dy = Y/(n-1)
    m = ceil(X/np.sqrt(r**2-dy**2/4) + 1)
    dx = X/(m-1)

    vx = np.linspace(xb,xe,m)
    vy = np.linspace(yb,ye,n)
    x,y = np.meshgrid(vx,vy)
    temp = np.ones((n,1))
    temp[n-1,:] = 0
    y = y + 0.5*dy*np.kron((1+(-1)**np.arange(1,m+1))/2,temp)

    np.reshape(x,(m*n,))
    np.reshape(y,(m*n,))
    tx = (np.arange(2,m+1,2)-1)*dx + xb
    np.reshape(tx,(len(tx),))
    ty = tx*0 + yb
    x = np.append(x,tx)
    y = np.append(y,ty)
    
    return np.column_stack((x,y))

def get_edges(tri):
    indices,indptr = tri.vertex_neighbor_vertices 
    
    #edges_b = [] 
    #edges_e = [] 
    edge_tuples = set() 

    for k in range(indices.shape[0]-1):
        for j in indptr[indices[k]:indices[k+1]]:
            try:
                if k <= j:
                    edge_tuples.add((k,j))
                else:
                    edge_tuples.add((j,k))
                #already_collected = list(zip(edges_b,edges_e))
                #print("already_collected = ", already_collected)
                #if (k,j) not in already_collected and (j,k) not in already_collected:
                #    edges_b.append(k)
                #    edges_e.append(j)
            except IndexError:
                pass
    edge_tuples = list(edge_tuples) 
    edges_b = [edg[0] for edg in edge_tuples]
    edges_e = [edg[1] for edg in edge_tuples]
    return np.column_stack((np.array(edges_b,dtype=int),
                            np.array(edges_e,dtype=int)))

def FindNextNode(NodesCoord,PrevNode,RefNode,TempNodes):
    P = NodesCoord[PrevNode,:]
    S = NodesCoord[RefNode,:]
    N = NodesCoord[TempNodes,:]

    NoOfTempNodes = TempNodes.shape[0]

    SP = np.ones((NoOfTempNodes,1))*(P-S)
    SN = N - np.ones((N.shape[0],1))*S
    LenSP = np.sqrt(SP[:,0]**2 + SP[:,1]**2)
    LenSN = np.sqrt(SN[:,0]**2 + SN[:,1]**2)
    
    DotProd = SP[:,0]*SN[:,0] + SP[:,1]*SN[:,1]
    Phi = np.arccos( DotProd / ( LenSP * LenSN ) )
    #print("Phi = ", Phi)
    #print("SP = ", SP)
    #print("SN = ", SN)
    #print("Cross-prod =", SP[:,0]*SN[:,1] - SP[:,1]*SN[:,0] )
    Temp = np.where( SP[:,0]*SN[:,1] - SP[:,1]*SN[:,0] < 0 )[0]
    #print("Temp = ", Temp) 
    Phi[Temp] = 2*np.pi-Phi[Temp]
    #print("Phi = ", Phi)

    return np.unravel_index(np.argmin(Phi,axis=None),Phi.shape)

def vis(NodesCoord,Edges,Quadrants,PhasesDiff):
    NoOfEdges = Edges.shape[0] 
    EdgesColor = np.zeros(NoOfEdges,dtype=int)
    EdgesColor[ np.logical_or(PhasesDiff == 2, PhasesDiff == np.nan) ] = 5 
    Temp = PhasesDiff == 0

    EdgesColor[Temp] = Quadrants[Edges[Temp,0]]
    EdgesColor = np.reshape(EdgesColor,((NoOfEdges,1)))

    vNaN = np.zeros(NoOfEdges)*np.nan

    EdgesXCoord = np.block([ np.reshape(NodesCoord[Edges[:,0],0],(NoOfEdges,1)),
                             np.reshape(NodesCoord[Edges[:,1],0],(NoOfEdges,1)),
                             np.reshape(vNaN,(NoOfEdges,1)) ]) 
    EdgesYCoord = np.block([ np.reshape(NodesCoord[Edges[:,0],1],(NoOfEdges,1)),
                             np.reshape(NodesCoord[Edges[:,1],1],(NoOfEdges,1)),
                             np.reshape(vNaN,(NoOfEdges,1)) ])  
    EdgesXCoord = np.reshape(EdgesXCoord,(3*NoOfEdges,1)) 
    EdgesYCoord = np.reshape(EdgesYCoord,(3*NoOfEdges,1))
    EdgesColor = np.kron(EdgesColor,np.reshape(np.array([1,1,1]),(3,1)))

    plt.figure() 
    plt.plot(EdgesXCoord[EdgesColor == 0],EdgesYCoord[EdgesColor == 0],color='k')
    plt.plot(EdgesXCoord[EdgesColor == 1],EdgesYCoord[EdgesColor == 1],color='r')
    plt.plot(EdgesXCoord[EdgesColor == 2],EdgesYCoord[EdgesColor == 2],color='y')
    plt.plot(EdgesXCoord[EdgesColor == 3],EdgesYCoord[EdgesColor == 3],color='g')
    plt.plot(EdgesXCoord[EdgesColor == 4],EdgesYCoord[EdgesColor == 4],color='b')
    plt.plot(EdgesXCoord[EdgesColor == 5],EdgesYCoord[EdgesColor == 5],color='m')
    plt.show()

def vis_contours(NoOfRegions,Regions,q):
    for k in range(1,NoOfRegions+1):
        plt.figure()
        #print("NodesCoord[Regions[k],:] = ", NodesCoord[Regions[k],:])
        plt.plot(NodesCoord[Regions[k],0],NodesCoord[Regions[k],1])
        plt.title("Contour {0}".format(k))
        plt.show()    
        #else:
            #print("NodesCoord[Regions[k],:] = ", NodesCoord[Regions[k],:])
            #for j in range(Regions[k].shape[0]):
            #    print("NodesCoord[Regions[k][j],0] = ", NodesCoord[Regions[k][j],0])
            #    print("Regions[k][j] = ", Regions[k][j])
            #    plt.plot(NodesCoord[Regions[k][:j+1],0],NodesCoord[Regions[k][:j+1],1])
            #    plt.show()
            #plt.savefig("j{0}".format(j))


def Poles_and_Roots(fun,param):

    ##########################################
    ### analysis parameters               ####
    ##########################################
    
    xb = param["xrange"][0]  # real part range begin 
    xe = param["xrange"][1]  # real part range end 
    yb = param["yrange"][0]  # imag part range begin
    ye = param["yrange"][1]  # imag part range end
    r = param["h"]           # initial mesh step

    NewNodesCoord = rect_dom(xb,xe,yb,ye,r) # initial mesh generation
    Tol = param["Tol"]  # accuracy
    visual = param["visual"]  # mesh visualization: 
                              # 0-turned off, 1-only last iteration,2-all iterations 
    ItMax = param["ItMax"]       # max number of iterations
    NodesMax = param["NodesMax"] # max number of nodes
    SkinnyTriangle = 3 # skinny triangle definition

    NrOfNodes = 0

    # general loop
    it = 0
    while it < ItMax and NrOfNodes < NodesMax:
        it +=1
        print("it = ", it) 
        if it == 1:
            NodesCoord = NewNodesCoord
            FunctionValues = np.zeros(len(NodesCoord),dtype=complex)
            Quadrants = np.zeros(len(NodesCoord))
        else:
            NodesCoord = np.block([[NodesCoord],[NewNodesCoord]])
            FunctionValues = np.block([FunctionValues, np.zeros(len(NewNodesCoord),dtype=complex)])
            Quadrants =  np.block([Quadrants,np.zeros(len(NewNodesCoord))])

        print("Evaluation of the function in {0} new points ...".format(NewNodesCoord.shape[0]))
       
        for Node in range(NrOfNodes,NrOfNodes+NewNodesCoord.shape[0]):
            z = NodesCoord[Node,0]+1j*NodesCoord[Node,1]
            FunctionValues[Node] = fun(z)
            Quadrants[Node] = vinq(FunctionValues[Node])

        NewNodesCoord = [] 
        NrOfNodes = NodesCoord.shape[0]
        print("Triangulation and analysis of {0} nodes ...".format(NrOfNodes))

        DT  = Delaunay(NodesCoord)
       

        #fig = delaunay_plot_2d(DT)
        #fig.show()
        #fig.clf() 
        #fig.savefig("DT-it{0}".format(it))

        Elements = DT.simplices
        Edges = get_edges(DT)

        NrOfElements = Elements.shape[0]

        PhaseDiff = np.mod(Quadrants[Edges[:,0]]-Quadrants[Edges[:,1]],4)
        #print("PhaseDiff = ", PhaseDiff.tolist())

        CandidateEdges = Edges[ np.logical_or(PhaseDiff==2,PhaseDiff==np.nan),:]
        #print("CandidateEdges = ", CandidateEdges)

        if CandidateEdges.shape[0] == 0:
            print("No roots in domain")
            return {} 

        Nodes1OfCandidateEdges = CandidateEdges[:,0]
        Nodes2OfCandidateEdges = CandidateEdges[:,1]

        CoordinatesOfNodes1OfCandidateEdges = NodesCoord[Nodes1OfCandidateEdges,:]
        CoordinatesOfNodes2OfCandidateEdges = NodesCoord[Nodes2OfCandidateEdges,:]

        CandidateEdgesLength = np.sqrt(np.sum((CoordinatesOfNodes2OfCandidateEdges-CoordinatesOfNodes1OfCandidateEdges)**2,axis=1))
        MinCandidateEdgesLength = np.min(CandidateEdgesLength) 
        MaxCandidateEdgesLength = np.max(CandidateEdgesLength) 
        print("Candidate edges length min: {0}, max: {1}".format(MinCandidateEdgesLength,MaxCandidateEdgesLength))

        if MaxCandidateEdgesLength < Tol:
            print("Assumed accuracy is achieved in iteration: ", it)
            print("---------------------------------------------------------------")
            break

        Temp = CandidateEdgesLength > Tol
        #print("Temp = ", Temp)
        ReduCandidateEdges = CandidateEdges[Temp,:]
        
        #print(" ReduCandidateEdges =",  ReduCandidateEdges )

        Temp = np.zeros((NrOfNodes,),dtype=int)
        Temp[ReduCandidateEdges[:,0]] = 1
        Temp[ReduCandidateEdges[:,1]] = 1
        CandidateNodes = np.where(Temp==1)[0]
       
        #print("CandidateNodes = ", CandidateNodes) 
         
        ArrayOfCandidateElements = [] 
        for Node in CandidateNodes:
            ArrayOfCandidateElements.extend([nr for nr,s in enumerate(DT.simplices) if Node in s])
        ArrayOfCandidateElements = np.array(ArrayOfCandidateElements,dtype=int)
        #print("ArrayOfCandidateElements = ", ArrayOfCandidateElements)  
        Temp = np.zeros((NrOfElements,),dtype=int)
        for k in range(ArrayOfCandidateElements.shape[0]):
            Temp[ArrayOfCandidateElements[k]] = Temp[ArrayOfCandidateElements[k]] +1

        IDOfFirstZoneCandidateElements = np.where(Temp >1)[0]
        #print("IDOfFirstZoneCandidateElements = ", IDOfFirstZoneCandidateElements)  
        IDOfSecondZoneCandidateElements = np.where(Temp == 1)[0]
       
        NoOfFirstZoneCandidateElements = IDOfFirstZoneCandidateElements.shape[0]
        if NoOfFirstZoneCandidateElements > 0: 
            FirstZoneCandidateElements = Elements[IDOfFirstZoneCandidateElements,:]

            #print("NoOfFirstZoneCandidateElements = ", NoOfFirstZoneCandidateElements) 
            #print("FirstZoneCandidateElements = ", FirstZoneCandidateElements)  
            
            TempExtraEdges = np.zeros((3*NoOfFirstZoneCandidateElements,2),dtype=int)
            for k in range(1,NoOfFirstZoneCandidateElements+1):
                TempExtraEdges[(k-1)*3+0,0] = FirstZoneCandidateElements[k-1,0] 
                TempExtraEdges[(k-1)*3+0,1] = FirstZoneCandidateElements[k-1,1] 
                TempExtraEdges[(k-1)*3+1,0] = FirstZoneCandidateElements[k-1,1] 
                TempExtraEdges[(k-1)*3+1,1] = FirstZoneCandidateElements[k-1,2] 
                TempExtraEdges[(k-1)*3+2,0] = FirstZoneCandidateElements[k-1,2] 
                TempExtraEdges[(k-1)*3+2,1] = FirstZoneCandidateElements[k-1,0] 
     
            NewNodesCoord = np.sum(NodesCoord[TempExtraEdges[0,:],:],axis=0)/2
            NewNodesCoord = NewNodesCoord.reshape((1,2))
            #print("NewNodesCoord = {0}, shape = {1}".format(   NewNodesCoord,NewNodesCoord.shape )) 
            for k in range(1,3*NoOfFirstZoneCandidateElements):
                CoordOfTempEdgeNode1 = NodesCoord[TempExtraEdges[k,0],:]
                CoordOfTempEdgeNode2 = NodesCoord[TempExtraEdges[k,1],:]
                TempNodeCoord = (CoordOfTempEdgeNode1+CoordOfTempEdgeNode2)/2
                TempEdgeLength = np.sqrt(np.sum((CoordOfTempEdgeNode2-CoordOfTempEdgeNode1)**2))

                if TempEdgeLength > Tol:
                    DistNodes = np.sqrt( (NewNodesCoord[:,0]-TempNodeCoord[0])**2 + (NewNodesCoord[:,1]-TempNodeCoord[1])**2  )
                    if np.all( DistNodes > 2*1e-15):
                        NewNodesCoord = np.block([[NewNodesCoord],[TempNodeCoord]])

            # removing the frist new node if the edge is too short
            CoordOfTempNode1 = NodesCoord[TempExtraEdges[0,0],:]
            CoordOfTempNode2 = NodesCoord[TempExtraEdges[0,1],:]
            TempEdgeLength = np.sqrt(np.sum((CoordOfTempNode2- CoordOfTempNode1 )**2))
            if TempEdgeLength < Tol:
                NewNodesCoord = NewNodesCoord[1:,:]
        else:
            NewNodesCoord = []

        NoOfSecondZoneCandidateElements = IDOfSecondZoneCandidateElements.shape[0]
        SecondZoneCandidateElements = Elements[IDOfSecondZoneCandidateElements,:]
       
        #print("SecondZoneCandidateElements = ",  SecondZoneCandidateElements)

        for k in range(NoOfSecondZoneCandidateElements):
            NodesInTempElement = SecondZoneCandidateElements[k,:] 
            Node1Coord = NodesCoord[NodesInTempElement[0],:]
            Node2Coord = NodesCoord[NodesInTempElement[1],:]
            Node3Coord = NodesCoord[NodesInTempElement[2],:]
            TempLengths = np.zeros((3,),dtype=float)
            TempLengths[0] = np.sqrt(np.sum((Node2Coord-Node1Coord)**2))
            TempLengths[1] = np.sqrt(np.sum((Node3Coord-Node2Coord)**2))
            TempLengths[2] = np.sqrt(np.sum((Node1Coord-Node3Coord)**2))
            #print("TempLengths = ", TempLengths)
            #print(" np.max(TempLengths) / np.min(TempLengths) = ", np.max(TempLengths) / np.min(TempLengths)) 
            if np.max(TempLengths) / np.min(TempLengths) > SkinnyTriangle:
                TempNodeCoord = (Node1Coord + Node2Coord + Node3Coord) / 3
                if k == 0 and NewNodesCoord == []:
                    NewNodesCoord = TempNodeCoord.reshape((1,2))
                else:
                    NewNodesCoord = np.block([[NewNodesCoord],[TempNodeCoord]])

        if visual == 2:
            vis(NodesCoord,Edges,Quadrants,PhaseDiff)

        print("Iteration: {0}, done".format(it))
        print("------------------------------------------------")

    if visual > 0:
        vis(NodesCoord,Edges,Quadrants,PhaseDiff)

    # Evaluation of regions and verification 
    print("Evaluation of regions and verification ...")
    TupleOfCandidateEdges = set( [ (a,b) for a,b in zip(CandidateEdges[:,0].tolist(),CandidateEdges[:,1].tolist()) ] )
    ArrayOfCandidateElements = []
    for k in range(DT.simplices.shape[0]):
        Node1 = DT.simplices[k,0] 
        Node2 = DT.simplices[k,1] 
        Node3 = DT.simplices[k,2]
        MyEdges = {(Node1,Node2),(Node2,Node1),(Node2,Node3),(Node3,Node2),(Node3,Node1),(Node1,Node3)}
        if MyEdges.intersection(TupleOfCandidateEdges) != set():
            ArrayOfCandidateElements.append(k)
    Temp = np.zeros(NrOfElements,dtype=int)
    for k in range(len(ArrayOfCandidateElements)):
        Temp[ArrayOfCandidateElements[k]] = 1

    IDOfCandidateElements = np.where(Temp==1)[0]
    NoOfCandidateElements = IDOfCandidateElements.shape[0]
    CandidateElements = Elements[IDOfCandidateElements,:]
    #print("CandidateElements = ", CandidateElements) 


    TempEdges = np.zeros((3*NoOfCandidateElements,2),dtype=int)
    for k in range(1,NoOfCandidateElements+1):
        TempEdges[(k-1)*3+0,0] = CandidateElements[k-1,0] 
        TempEdges[(k-1)*3+0,1] = CandidateElements[k-1,1] 
        TempEdges[(k-1)*3+1,0] = CandidateElements[k-1,1] 
        TempEdges[(k-1)*3+1,1] = CandidateElements[k-1,2] 
        TempEdges[(k-1)*3+2,0] = CandidateElements[k-1,2] 
        TempEdges[(k-1)*3+2,1] = CandidateElements[k-1,0] 

    # reduction of edges to contour
    MultiplicationOfTempEdges = np.zeros(3*NoOfCandidateElements)
    RevTempEdges = np.fliplr(TempEdges)
    for k in range(3*NoOfCandidateElements):
        if MultiplicationOfTempEdges[k] == 0:
            NoOfEdge = np.where( np.logical_and(RevTempEdges[:,0] == TempEdges[k,0],RevTempEdges[:,1] == TempEdges[k,1]))[0]
            if NoOfEdge.size == 0:
                MultiplicationOfTempEdges[k] = 1
            else:
                MultiplicationOfTempEdges[k] = 2
                MultiplicationOfTempEdges[NoOfEdge] = 2

    ContourEdges = TempEdges[MultiplicationOfTempEdges == 1,:]
    NoOfContourEdges = ContourEdges.shape[0]
    #print("ContourEdges = ", ContourEdges)

    # evaluation of regions
    NoOfRegions = 1
    Regions = {}
    Regions[NoOfRegions] = np.array([ContourEdges[0,0]])
    RefNode = ContourEdges[0,1]
    ContourEdges = ContourEdges[1:,:]
    while ContourEdges.shape[0] > 0:
        IndexOfNextEdge = np.where(ContourEdges[:,0] == RefNode)[0]

        if IndexOfNextEdge.size == 0:
            Regions[NoOfRegions] = np.block([[Regions[NoOfRegions]],[RefNode]])
            if ContourEdges.shape[0] > 0:
                NoOfRegions +=1
                Regions[NoOfRegions] = np.array([ContourEdges[0,0]])
                RefNode = ContourEdges[0,1]
                ContourEdges = ContourEdges[1:,:]
        else:
            if IndexOfNextEdge.shape[0] > 1:
                #print("IndexOfNextEdge = ", IndexOfNextEdge) 
                #print("Regions = ", Regions)
                #print(" Regions[NoOfRegions] = ", Regions[NoOfRegions])
                PrevNode = Regions[NoOfRegions][-1]
                TempNodes = ContourEdges[IndexOfNextEdge,1]
                Index = FindNextNode(NodesCoord,PrevNode,RefNode,TempNodes)
                IndexOfNextEdge = IndexOfNextEdge[Index]

            NextEdge = ContourEdges[IndexOfNextEdge,:]
            Regions[NoOfRegions] = np.block([[Regions[NoOfRegions]],[ ContourEdges[IndexOfNextEdge,0]]])
            RefNode = ContourEdges[IndexOfNextEdge,1]
            ContourEdges = np.delete(ContourEdges,IndexOfNextEdge,0)

    Regions[NoOfRegions] = np.block([[Regions[NoOfRegions]],[RefNode]])
    #print("Regions = ", Regions)

    q = np.zeros(NoOfRegions)
    z = np.zeros(NoOfRegions,dtype=complex)
    for k in range(1,NoOfRegions+1):
        QuadrantSequence = Quadrants[Regions[k]]
        #print("QuadrantSequence = ", QuadrantSequence ) 
        dQ = QuadrantSequence[1:] - QuadrantSequence[:-1]
        dQ[dQ == 3] = -1
        dQ[dQ == -3] = 1
        #print("np.abs(dQ) == 2 =",  np.abs(dQ) == 2)
        #print("dQ[np.abs(dQ) == 2] = ", dQ[np.abs(dQ) == 2])  
        dQ[np.abs(dQ) == 2] = np.nan
        #print("dQ = ", dQ)
        q[k-1] = np.sum(dQ)/4
        z[k-1] = np.mean(NodesCoord[Regions[k],0] + 1j*NodesCoord[Regions[k],1])
        #print("Region {0}, z = {1}, with q = {2}".format(k,z[k-1],q[k-1]))

    z_root = z[q>0]
    z_roots_multiplicity = q[q>0]
    NoOfRoots = z_root.shape[0]
    print("-------------------------------------")
    print("Root and its multiplicity")
    for zi,qi in zip(z_root,z_roots_multiplicity):
        print("z = {0}, with multiplicity = {1}".format(zi,qi))

    z_poles = z[q<0]
    z_poles_multiplicity = q[q<0]
    NoOfPoles = z_poles.shape[0]
    print("-------------------------------------")
    print("Poles and its multiplicity:")
    for zi,qi in zip(z_poles,z_poles_multiplicity):
        print("z = {0}, with multiplicity = {1}".format(zi,qi))

    if visual > 1:
        vis_contours(NoOfRegions,Regions,q)

    return {"roots": z_root,
            "roots_multiplicity": z_roots_multiplicity,
            "poles": z_poles,
            "poles_multiplicity": z_poles_multiplicity
            }

