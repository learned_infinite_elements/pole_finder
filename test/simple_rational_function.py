from GRPF import Poles_and_Roots 

param = {}
# rectangular domain definition z=x+jy x\in[xb,xe], y \in [yb,ye]
param["xrange"] = [-2,2]
param["yrange"] = [-2,2]
param["h"] = 0.1 
param["Tol"] = 1e-9
param["visual"] = 1 
param["ItMax"] = 10
param["NodesMax"] = 500000 

def fun(z):
    result = (z-1)*(z-1j)**3*(z+1)**1/(z+1j)
    return result 

result = Poles_and_Roots(fun,param)
